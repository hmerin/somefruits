<?php

namespace Olapic\PlatformTeamTest\Infrastructure\MySQL;

use Doctrine\DBAL\Connection;
use Exception;
use PDO;

class FruitRepository
{
    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getRandomFruit()
    {
        $q = $this->db->createQueryBuilder()
            ->select('*')
            ->from('fruits')
            ->orderBy('rand()')
            ->setMaxResults(1);

        $fruit = $q->execute()->fetch(PDO::FETCH_ASSOC);

        // if (empty($data)) {
        //     throw new EntityNotFoundException($medium_id);
        // }

        return $fruit;
    }
}
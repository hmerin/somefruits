# Platform Team Test

This can be run locally using [docker compose](https://docs.docker.com/compose/).

# Build base image

Before bringing up the development environment, we need to build the base PHP webserver image. We could have pushed it to a registry but instead we leave it up to the developer to create it from scratch.

```bash
docker build -t php_webapp:latest -f Dockerfile.build .
```

# Running the application

```bash
docker-compose up
```

# Making changes to the application

After making changes to the source code of the application, it can be stopped and rebuilt by doing:

```bash
docker-compose stop web
docker-compose up --build -d web
```

# Testing the application

You can use `docker-compose` to run the unit and integration test scripts over the Docker image artifact

Unit tests:

```bash
docker-compose run --rm web ./tools/build/unit-test.sh
```

Integration tests:

```bash
docker-compose run --rm web ./tools/build/integration-test.sh
```

Since the `web` service depends on the `db` service, the `db` service will be brought up before
running those tests. After finishing you can clean it up running:

```bash
docker-compose stop db && docker-compose rm -f db
```

## Production deployments

It will require some jobs and ENV settings in your CI to accomplish the deployment successfully:

ENVS to set in your CI:
    * REGISTRY_HOST
    * CI_REGISTRY_USER (only if you are not using gitlabCI)
    * CI_REGISTRY_PASSWORD (only if you are not using gitlabCI)
    * DB_HOST
    * DB_USER
    * DB_NAME
    * DB_PASSWORD
    * PROD_DOMAIN_SWARM
    * PROD_USER_SWARM
    * PROD_DOMAIN_K8S
    * PROD_USER_K8S

### Deploy to production using swarm

Assuming a swarm cluster is created before, there are some steps listed to integrate this deployment in your preferred CI/CD.

* Lint Job:

    Check the stack and compose files syntax before trigger any other job.

* Build Job:

    Build the docker images and push them to a private registry.

* Deploy Job:

    Run a deploy script which runs the deploy command in the production server.

### Deploy to production using kubernetes

Assuming a k8s cluster is created before, there are some steps to integrate this deployment to your CI/CD.

Create a secret for your registry credentials:

1. Make a docker login in your host

2. Create a secret based on this registry credentials to be used in k8s: `kubectl create secret generic regcred --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson`

* Build Job:

    Build the docker images and push them to a private registry.

* Deploy Job:

    deploy changes from fruits.yml file

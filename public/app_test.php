<?php

require __DIR__.'/../vendor/autoload.php';

if ('cli-server' === php_sapi_name() && isset($_SERVER['REQUEST_URI'])) {
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $path = urldecode($path);
    $file = __DIR__.$path;
    if ($path !== '/' && file_exists($file)) {
        return false;
    }
}

// Quick Fix to remove the trailing slash from URL's
// may nee some extra review or we may move this into Silex
// kept here for performance reasons at the moment.
if (isset($_SERVER['REQUEST_URI'])) {
    $url = parse_url($_SERVER['REQUEST_URI']);
    if (!empty($url['path']) && '/' !== $url['path']) {
        $_SERVER['REQUEST_URI'] = str_replace(
            $url['path'],
            rtrim($url['path'], '/'),
            $_SERVER['REQUEST_URI']
        );
    }
}

ini_set('xdebug.var_display_max_data', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_depth', -1);

use Symfony\Component\Debug\Debug;
use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

error_reporting(-1);
Debug::enable();

$app          = new Olapic\PlatformTeamTest\HttpApi\Application();
$app['debug'] = filter_var(getenv('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

$app->run();

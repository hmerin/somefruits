#!/bin/bash

# Get base directory of this script
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Invoke phpunit, which lives inside the vendor directory, two directories up in the hierarchy
"${BASE_DIR}"/../../vendor/bin/phpunit --colors --stderr $*

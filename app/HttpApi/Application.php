<?php

namespace Olapic\PlatformTeamTest\HttpApi;

use Olapic\Config\ConfigInstance;
use Olapic\PlatformTeamTest\Infrastructure;
use Olapic\PlatformTeamTest\HttpApi\Controller;
use Silex;

class Application extends Silex\Application
{
    public function __construct()
    {
        parent::__construct();

        $this->registerConfig();
        $this->registerProviders();
        $this->registerServices();
        $this->registerRepositories();
        $this->registerRoutes();
    }

    protected function registerConfig()
    {
        $this['config'] = [
            'name' => getenv('APP_NAME'),
            'env'  => getenv('APP_ENV'),
            'db'   => [
                'dbs.options' => [
                    'platform_team_test' => [
                        'driver'   => getenv('DB_DRIVER'),
                        'host'     => getenv('DB_HOST'),
                        'dbname'   => getenv('DB_NAME'),
                        'user'     => getenv('DB_USER'),
                        'password' => getenv('DB_PASSWORD'),
                        'charset'  => getenv('DB_CHARSET'),
                    ],
                ],
            ],
        ];
    }

    protected function registerProviders()
    {
        $this->register(
            new Silex\Provider\DoctrineServiceProvider(),
            $this['config']['db']
        );
    }

    protected function registerServices()
    {

    }

    protected function registerRepositories()
    {
        $this['repository.fruit'] = $this->share(function ($this) {
            return new Infrastructure\MySQL\FruitRepository(
                $this['dbs']['platform_team_test']
            );
        });
    }

    protected function registerRoutes()
    {
        $this->mount('/service', new Controller\ServiceController());
        $this->mount('/fruits', new Controller\FruitController());
    }
}

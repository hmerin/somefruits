<?php

namespace Olapic\PlatformTeamTest\HttpApi\Controller;

use Exception;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class FruitController implements ControllerProviderInterface
{
    protected $app;

    public function connect(Application $app)
    {
        $this->app   = $app;
        $controllers = $app['controllers_factory'];

        $controllers
            ->get('/random', [$this, 'getRandomFruit'])
            ->bind('random_fruit');

        return $controllers;
    }

    public function getRandomFruit()
    {
        return new JsonResponse(
            [
                'status' => 200,
                'fruit'  => $this->app['repository.fruit']->getRandomFruit(),
            ]
        );
    }
}
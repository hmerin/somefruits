<?php

namespace Olapic\PlatformTeamTest\HttpApi\Controller;

use Exception;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ServiceController implements ControllerProviderInterface
{
    protected $app;

    public function connect(Application $app)
    {
        $this->app   = $app;
        $controllers = $app['controllers_factory'];

        $controllers
            ->get('/health', [$this, 'getHealth'])
            ->bind('service_health');

        return $controllers;
    }

    public function getHealth(Application $app)
    {
        try {
            $revision_file    = sprintf('%s/%s', realpath(__DIR__.'/../..'), '.revision');
            $version['build'] = exec(sprintf('cd %s; /usr/bin/git rev-parse HEAD;', __DIR__));
            $version['git']   = file_exists($revision_file) ? trim(file_get_contents($revision_file)) : 'error';
        } catch (Exception $e) {
            $version = null;
        }

        $data = [
            'isHealthy' => true,
            'status'    => 200,
            'env'       => $app['config']['env'],
            'version'   => $version,
        ];

        return new JsonResponse([
            'data' => $data,
        ], $data['status']);
    }
}

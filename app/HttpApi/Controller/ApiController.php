<?php

namespace Olapic\PlatformTeamTest\HttpApi\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

abstract class ApiController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $collection = $app['controllers_factory'];

        return $this->getControllerCollection($collection);
    }

    abstract protected function getControllerCollection(ControllerCollection $collection);
}
